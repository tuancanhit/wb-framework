<?php

namespace William\Wb\Controller\Home;

use William\Base\Api\PageResponse\Response;
use William\Base\Block\AbstractBlock;
use William\Base\Block\Renderer;
use William\Base\Controller\AbstractFrontendController;
use William\Base\Controller\Request;
use William\Product\Model\Product;
use William\Wb\Block\LoginForm;

/**
 * Class Index
 *
 * @package William\Wb\Home
 */
class Login extends AbstractFrontendController
{
    public const IS_AJAX_RENDER = true;
    protected bool $addHeader   = true;
    protected bool $addFooter   = true;

    /**
     * @param Request  $request
     * @param Response $response
     * @param Renderer $renderer
     * @param Product  $product
     */
    public function __construct(Request $request, Response $response, Renderer $renderer, protected Product $product)
    {
        $this->request = $request;
        $this->response = $response;
        $this->renderer = $renderer;
        parent::__construct($request, $response, $renderer);
    }

    /**
     * @return AbstractBlock
     * @throws \ReflectionException
     */
    public function execute()
    {
        return $this->renderer->createBlock(LoginForm::class)->setVars([
            'title' => 'Login Page'
        ]);
    }
}