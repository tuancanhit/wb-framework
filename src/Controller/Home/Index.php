<?php

namespace William\Wb\Controller\Home;

use William\Base\Api\PageResponse\Response;
use William\Base\Controller\AbstractFrontendController;
use William\Wb\Block\LoginForm;

/**
 * Class Index
 *
 * @package William\Wb\Home
 */
class Index extends AbstractFrontendController
{
    public const IS_AJAX_RENDER = true;
    protected bool $addHeader = true;
    protected bool $addFooter = true;

    /**
     * @return Response
     * @throws \Exception
     */
    public function execute()
    {
        /** @var Response $response */
        $response = $this->response->create();
        return $response->setTemplate('@app::base.php');
    }
}