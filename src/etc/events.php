<?php

return [
    'core_after_render' => [],
    'block_login_form_get_vars_after' => [
        \William\Base\Observer\Security\FormKeyListener::class
    ]
];