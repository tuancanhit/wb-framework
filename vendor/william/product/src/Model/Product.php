<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Product\Model;

use William\Base\Model\AbstractCollection;
use William\Base\Model\AbstractResourceModel;

/**
 * Class Product
 *
 * @package William\Product\Model
 */
class Product extends \William\Base\Model\AbstractModel
{
    /** @var string */
    protected string $table = 'product';

    /** @var string */
    protected string $primary = 'id';

    /** @var string */
    protected string $class = \William\Product\Model\Product::class;

    /** @var string  */
    protected string $resourceModelClass = \William\Product\Model\ResourceModel\Product::class;

    protected string $collectionClass = \William\Product\Model\ResourceModel\Product\Collection::class;

    /**
     * @return \William\Product\Model\ResourceModel\Product|AbstractResourceModel
     */
    public function getResourceModel()
    {
        return parent::getResourceModel();
    }
}