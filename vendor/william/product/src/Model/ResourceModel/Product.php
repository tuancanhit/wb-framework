<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Product\Model\ResourceModel;

use William\Base\Model\AbstractResourceModel;

/**
 * Class Product
 *
 * @package William\Product\Model\ResourceModel
 */
class Product extends AbstractResourceModel
{

}