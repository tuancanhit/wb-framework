<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Product\Controller;

use William\Base\Api\PageResponse\Response;
use William\Base\Api\PageResponse\ResponseInterface;
use William\Base\Block\BlockInterface;
use William\Base\Block\Renderer;
use William\Base\Controller\AbstractFrontendController;
use William\Base\Controller\Request;

/**
 * Class ProductDetailController
 */
class ProductDetailController extends AbstractFrontendController
{
    /**
     * @return ResponseInterface|BlockInterface
     * @throws \ReflectionException
     */
    public function execute()
    {
        return parent::execute();
    }
}