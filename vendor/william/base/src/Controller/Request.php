<?php

namespace William\Base\Controller;

use William\Base\Api\RequestInterface;
use William\Base\Helper\ScopeResolver;
use William\Base\Model\AbstractInstance;
use William\Base\Route\Router;

/**
 * Class Request
 * @method array getServer()
 * @method array getCookie()
 * @method array getRequest()
 *
 * @package William\Base\Controller
 */
class Request extends AbstractInstance implements RequestInterface
{
    const CLI    = 'CLI';
    const GET    = 'GET';
    const POST   = 'POST';
    const PUT    = 'PUT';
    const DELETE = 'DELETE';

    /** @var array */
    protected array $request = [];

    /** @var array  */
    protected array $scopes = [];

    /**  */
    public function __construct()
    {
        $this->request = $_REQUEST;
        $data = [
            'server'  => $_SERVER,
            'cookie'  => $_COOKIE,
            'request' => $_REQUEST,
            'method'  => $this->getMethod(),
        ];
        return parent::__construct($data);
    }

    /**
     * @param string $key
     * @param        $default
     * @return mixed|null
     */
    public function getParam(string $key, $default = null)
    {
        if (!isset($this->request[$key])) {
            return $default;
        }
        return $this->request[$key];
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->request;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        if ($this->isCli()) {
            return self::CLI;
        }
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }

    /**
     * @return bool
     */
    public function isCli()
    {
        return strtoupper(PHP_SAPI) == self::CLI;
    }

    /**
     * @return string|null
     */
    public function getRequestPath()
    {
        $requestUri = $this->getServer()['REQUEST_URI'];
        $pattern = '/^[^?]+/';
        if (preg_match($pattern, $requestUri, $matches)) {
            if (empty($matches[0])) {
                return null;
            }
            return $matches[0];
        }
        return null;
    }

    /**
     * @return string
     */
    public function getFullPath()
    {
        return Router::buildFullPath($this->getRequestPath());
    }

    /**
     * @return string
     */
    public function getScope()
    {
        if ($this->isCli()) {
            return ScopeResolver::IS_CLI;
        }
        $fullPath = $this->getFullPath();
        if (empty($this->scopes[$fullPath])) {
            $this->scopes[$fullPath] = $this->isAdmin() ? ScopeResolver::IS_ADMIN : ScopeResolver::IS_FRONT;
        }
        return $this->scopes[$fullPath];
    }

    /**
     * @return string
     */
    public function getUserIpAddress()
    {
        $ip = 'NOT_FOUND';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        if ($this->isCli()) {
            return false;
        }
        return strtolower(current(explode('/', $this->getFullPath()))) == strtolower(config('admin.frontName', 'admin'));
    }

    /**
     * @return mixed|null
     */
    public function getIsAjaxRequestRender()
    {
        return $this->getParam('is_ajax_request_render', false);
    }
}