<?php

namespace William\Base\Controller;

use William\Base\Api\PageResponse\Response;
use William\Base\Api\PageResponse\ResponseInterface as PageResponseInterface;
use William\Base\Api\RequestInterface;
use William\Base\Api\RequestResponse\ResponseInterface as RequestResponseInterface;
use William\Base\Block\BlockInterface;
use William\Base\Block\Renderer;

/**
 * Class AbstractController
 *
 * @package William\Base\Controller
 */
abstract class AbstractController implements \William\Base\Api\Controller\AbstractControllerInterface
{
    /** @var string */
    protected string $redirect = '';

    /** @var string */
    protected string $prefix = '';

    public const IS_AJAX_RENDER = false;
    protected bool $addHeader = false;
    protected bool $addFooter = false;
    protected bool $addNav = false;

    /**
     * @param Request  $request
     * @param Response $response \
     * @param Renderer $renderer
     */
    public function __construct(
        protected Request  $request,
        protected Response $response,
        protected Renderer $renderer
    )
    {
    }

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return string
     */
    public function getRedirect(): string
    {
        if (!$this->redirect) {
            return '';
        }
        return urlBuild($this->redirect);
    }

    /**
     * @param string $redirect
     * @return $this
     */
    public function setRedirect(string $redirect)
    {
        $this->redirect = $redirect;
        return $this;
    }

    /**
     * @return PageResponseInterface|RequestResponseInterface|BlockInterface
     */
    abstract public function execute();

    /**
     * @return string
     */
    protected function getEventPrefix()
    {
        if (!$this->prefix) {
            $this->prefix = str_replace('/', '_', $this->request->getFullPath());
        }
        return $this->prefix;
    }

    /**
     * @return void
     */
    protected function beforeExecute(array $events = [])
    {
        $functions = [
            $this->getEventPrefix() . '_before_execute'
        ];
        $functions = array_merge($functions, $events);
        foreach ($functions as $func) {
            if (function_exists($func) && is_callable($func)) {
                $func($this);
            }
        }
    }

    /**
     * @return void
     */
    protected function afterExecute(array $events = [])
    {
        $functions = [
            $this->getEventPrefix() . '_after_execute'
        ];
        $functions = array_merge($functions, $events);
        foreach ($functions as $func) {
            if (function_exists($func) && is_callable($func)) {
                $func($this);
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function launch()
    {
        $this->beforeExecute();
        $result = $this->execute();
        $this->afterExecute();

        if ($this->getRedirect()) {
            header(sprintf('Location: %s', $this->getRedirect()), true, 302);
            exit();
        }

        if ($result instanceof PageResponseInterface || $result instanceof BlockInterface) {
            $result
                ->setData('add_header', $this->getRequest()->getParam('add_header', $this->addHeader))
                ->setData('add_footer', $this->getRequest()->getParam('add_footer', $this->addFooter));
            return $result->toHtml();
        }

        if ($result instanceof RequestResponseInterface) {
            return $result->makeResponse();
        }

        throw new \Exception('Register page is incorrect');
    }
}