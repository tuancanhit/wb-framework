<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Controller\Ui;

use William\Base\Controller\AbstractFrontendController;

/**
 * Class AjaxRenderController
 */
class AjaxRenderController extends AbstractFrontendController
{
    protected bool $addHeader = true;
    protected bool $addFooter = true;

    public function execute()
    {
        return $this->response->create()->setVars([
            'ui_render_url' => urlBuild($this->getRequest()->getFullPath(), [
                'is_ajax_request_render' => 1,
                'add_header' => 0,
                'add_footer' => 0
            ])
        ])->setTemplate('@core::ui/ajax-render/base.php');
    }
}