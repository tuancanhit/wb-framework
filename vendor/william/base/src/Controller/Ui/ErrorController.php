<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Controller\Ui;

use William\Base\Api\PageResponse\ResponseInterface as PageResponseInterface;
use William\Base\Api\RequestResponse\ResponseInterface as RequestResponseInterface;
use William\Base\Block\BlockInterface;
use William\Base\Controller\AbstractFrontendController;

/**
 * Class AjaxRenderController
 */
class ErrorController extends AbstractFrontendController
{
    protected string $message = '';

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return PageResponseInterface|RequestResponseInterface|BlockInterface
     * @throws \ReflectionException
     */
    public function execute()
    {
        return $this->response->create()->setVars(['message' => $this->message])->setTemplate('@core::error.php');
    }
}