<?php

namespace William\Base\Controller;

use William\Base\Api\PageResponse\ResponseInterface;
use William\Base\Api\PageResponse\ResponseInterface as PageResponseInterface;
use William\Base\Api\RequestResponse\ResponseInterface as RequestResponseInterface;
use William\Base\Block\BlockInterface;

/**
 * Class AbstractFrontendController
 *
 * @package William\Base\Controller
 */
class AbstractFrontendController extends AbstractController
{
    /**
     * @param array $events
     * @return void
     */
    protected function beforeExecute(array $events = [])
    {
        $events = [
            'abstract_frontend_controller_before_execute'
        ];
        parent::beforeExecute($events);
    }

    /**
     * @param array $events
     * @return void
     */
    protected function afterExecute(array $events = [])
    {
        $events = [
            'abstract_frontend_controller_after_execute'
        ];
        parent::beforeExecute($events);
    }

    /**
     * @return PageResponseInterface|RequestResponseInterface|BlockInterface
     * @throws \ReflectionException
     */
    public function execute()
    {
        /** @var ResponseInterface $response */
        $response = $this->response->create();
        return $response->setVars([])->setTemplate('@core::blank.php');
    }
}