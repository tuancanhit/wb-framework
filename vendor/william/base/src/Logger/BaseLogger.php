<?php
declare(strict_types=1);

namespace William\Base\Logger;

use Monolog\Logger as MonoLogger;
use Monolog\Handler\StreamHandler;

/**
 * Class BaseLogger
 *
 * @package William\Base\Logger
 */
class BaseLogger
{
    const LOG_DEBUG = \Monolog\Logger::DEBUG;
    const LOG_ERROR = \Monolog\Logger::ERROR;
    const LOG_WARN = \Monolog\Logger::WARNING;

    /** @var string */
    protected string $type = 'base';

    /** @var string */
    protected string $path = 'var/log/base.log';

    /**
     * @param MonoLogger|null $logger
     */
    public function __construct(protected ?MonoLogger $logger = null)
    {
        if (null == $this->logger) {
            $this->logger = new MonoLogger($this->type);
            $this->logger = $this->logger->pushHandler(new StreamHandler($this->getFullPath(), self::LOG_DEBUG));
        }
    }

    /**
     * @return string
     */
    protected function getFullPath()
    {
        return sprintf('%s%s', config('root_folder'), $this->path);
    }

    /**
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function debug(string $message, array $context = [])
    {
        $this->logger->debug(...func_get_args());
    }

    /**
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function warn(string $message, array $context = [])
    {
        $this->logger->warning(...func_get_args());
    }

    /**
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function error(string $message, array $context = [])
    {
        $this->logger->error(...func_get_args());
    }

    /**
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function info(string $message, array $context = [])
    {
        $this->logger->info(...func_get_args());
    }

    /**
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function critical(string $message, array $context = [])
    {
        $this->logger->critical(...func_get_args());
    }
}