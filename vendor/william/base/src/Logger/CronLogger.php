<?php
declare(strict_types=1);

namespace William\Base\Logger;

use Monolog\Logger as MonoLogger;
use Monolog\Handler\StreamHandler;

/**
 * Class BaseLogger
 *
 * @package William\Base\Logger
 */
class CronLogger extends BaseLogger
{
    protected string $path = 'var/log/cron.log';
}