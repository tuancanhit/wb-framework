<?php

declare(strict_types=1);

namespace William\Base\Cron\Indexer;

use William\Base\Api\Cron\CronInterface;
use William\Base\Logger\CronLogger;

/**
 * Class Product
 *
 * @package William\Base\Cron\Indexer
 */
class Product implements CronInterface
{
    /**
     * @var CronLogger
     */
    protected CronLogger $logger;

    /**
     * @param CronLogger $logger
     */
    public function __construct(CronLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $this->logger->info('Cron run');
    }

    /**
     * @inheritDoc
     */
    public function isCompleted()
    {
        // TODO: Implement isCompleted() method.
    }

    /**
     * @inheritDoc
     */
    public function isFailure(?\Throwable $e = null)
    {
        // TODO: Implement isFailure() method.
    }

    /**
     * @inheritDoc
     */
    public static function getSchedule(): string
    {
        return '* * * * *';
    }

    /**
     * @return string
     */
    public function getCronIdentifier(): string
    {
        return 'index_product';
    }
}