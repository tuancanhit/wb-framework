<?php

namespace William\Base\Pool;

use William\Base\Exception\InstanceNotFoundException;

/**
 * Class AbstractPool
 *
 * @package William\Base\Pool
 */
class AbstractPool
{
    public static $_instance = null;

    /**
     * @var array
     */
    protected array $objects = [];

    /**
     * @return $this
     */
    public static function getInstance(array $args = [])
    {
        if (null == self::$_instance) {
            self::$_instance = new self(...func_get_args());
        }
        return self::$_instance;
    }

    /**
     * @param string $class
     * @param object $objects
     * @return $this
     */
    public function addObject(string $class, $objects)
    {
        $this->objects[$class] = $objects;
        return $this;
    }

    /**
     * @param string $class
     * @param bool   $throwable
     * @return mixed
     * @throws InstanceNotFoundException
     */
    public function getObject(string $class, bool $throwable = true)
    {
        if (empty($this->objects[$class])) {
            if (!$throwable) {
                return null;
            }
            throw new InstanceNotFoundException(
                sprintf('Not found instance of %s in pool', $class)
            );
        }
        return $this->objects[$class];
    }
}