<?php

namespace William\Base\Api;


use William\Base\Block\Renderer;
use William\Base\Helper\DependencyResolver;
use William\Base\Helper\TemplateResolver;
use William\Base\Model\AbstractInstance;
use William\Base\Observer\Event;

/**
 * Class AbstractResponse
 *
 * @package William\Base\Api
 */
class AbstractResponse extends AbstractInstance
{
    /** @var array */
    protected array $templates = [];

    /**
     * @param TemplateResolver $templateResolver
     * @param Event            $event
     * @param Renderer         $blockRenderer
     * @param array            $data
     */
    public function __construct(
        protected TemplateResolver $templateResolver,
        protected Event            $event,
        protected Renderer         $blockRenderer,
        array                      $data = []
    )
    {
        parent::__construct($data);
    }

    /**
     * @param string $helper
     * @return mixed|object|null
     * @throws \ReflectionException
     */
    public function helper(string $helper)
    {
        return DependencyResolver::getInstance()->resolve($helper);
    }

    /**
     * @param string $block
     * @return false|string
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function requireBlock(string $block)
    {
        return $this->blockRenderer->createBlock($block)->toHtml();
    }

    /**
     * @return string
     * @throws \ReflectionException|\Exception
     */
    public function getTemplatePath(string $template)
    {
        if (!isset($this->templates[$template])) {
            $this->templates[$template] = $this->templateResolver->resolve($template);
        }
        return $this->templates[$template];
    }

    /**
     * @return array
     */
    protected function getBaseVars()
    {
        return [
            'block' => $this,
            'baseUrl' => config('site.base_url'),
        ];
    }

    /**
     * @return array
     */
    public function getChildBlocks(string $blockName = '')
    {
        $blocks = $this->getData('child_blocks') ?: [];
        if ($blockName && !empty($blocks[$blockName])) {
            return [$blockName => $blocks[$blockName]];
        }
        return $blocks;
    }

    /**
     * @param string $blockName
     * @param string $block
     * @return $this
     */
    public function addChildBlock(string $blockName, string $block)
    {
        $childBlocks = $this->getData('child_blocks') ?: [];
        $childBlocks[$blockName][] = $block;
        $this->setData('child_blocks', $childBlocks);
        return $this;
    }

    /**
     * @return array|mixed|null
     */
    public function getVars()
    {
        $vars = $this->getData('vars');
        if (null == $vars) {
            $vars = [];
        }
        return $vars;
    }


    /**
     * @return false|string
     * @throws \Throwable
     */
    public function toHtml()
    {
        $this->beforeToHtml();
        $template = $this->getTemplate();
        if (!$template) {
            return '';
        }
        try {
            $vars = array_merge($this->getVars(), $this->getBaseVars());
            extract($vars);
            ob_start();
            if (!is_array($template)) {
                include $this->getTemplatePath($template);
            } else {
                foreach ($template as $tmpl) {
                    include $this->getTemplatePath($tmpl);
                }
            }
            $this->afterToHtml();
            return ob_get_clean();
        } catch (\Exception|\Throwable|\TypeError $e) {
            ob_end_clean();
            throw $e;
        }
    }

    /**
     * @param string $blockName
     * @return array|string
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function getChildBlocksObject(string $blockName = '')
    {
        $blocksHtml = [];
        foreach ($this->getChildBlocks($blockName) as $index => $block) {
            if (!is_array($block)) {
                $block = [$block];
            }
            $blocksHtml[$index] = array_map(function ($cl) {
                return $this->blockRenderer->createBlock($cl);
            }, $block);
        }
        if ($blockName) {
            return $blocksHtml[$blockName] ?? [];
        }
        return $blocksHtml;
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    protected function beforeToHtml()
    {
        $template = $this->getTemplate();
        if (!is_array($template)) {
            $template = [$template];
        }

        if (!$this->getData('remove_nav')) {
            $this->event->dispatch('set_nav_vars', ['object' => $this]);
            array_unshift($template, '@core::element/nav.php');
        }

        if ($this->getData('add_header')) {
            $this->event->dispatch('set_header_vars', ['object' => $this]);
            array_unshift($template, '@core::element/header.php');
        }

        if ($this->getData('add_footer')) {
            $this->event->dispatch('set_footer_vars', ['object' => $this]);
            $template[] = '@core::element/footer.php';
        }
        $this->setTemplate($template);
    }

    /**
     * @return void
     */
    protected function afterToHtml(){}

    /**
     * @param array $vars
     * @return $this
     */
    public function setVars(array $vars = [])
    {
        $this->setData('vars', $vars);
        return $this;
    }

    /**
     * @param string|array $template
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->setData('template', $template);
        return $this;
    }

    /**
     * @return string|array
     */
    public function getTemplate()
    {
        return $this->getData('template');
    }
    public function getChildBlocksHtml(string $blockName = ''){}
}