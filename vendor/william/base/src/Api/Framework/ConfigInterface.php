<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Api\Framework;

/**
 * Interface ConfigInterface
 *
 * @api
 * @package William\Base\Api\Framework
 */
interface ConfigInterface
{
    function config(string $path = '');
    function urlBuild(string $route, array $params = []);
}