<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Api\Framework;

use William\Base\Model\AbstractInstance;
use William\Base\Model\DataObject;
use William\Base\Observer\Listener;

/**
 * Interface MiddlewareInterface
 *
 * @api
 * @package William\Base\Api\Framework
 */
interface ListenerInterface
{
    /**
     * @param array|null $args
     * @return mixed
     */
    public function execute(array &$args = null);

    /**
     * @return string
     */
    public function getEvent(): string;

    /**
     * @param string $event
     * @return $this
     */
    public function setEvent(string $event);
}