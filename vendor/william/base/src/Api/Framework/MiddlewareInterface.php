<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Api\Framework;

/**
 * Interface MiddlewareInterface
 *
 * @api
 * @package William\Base\Api\Framework
 */
interface MiddlewareInterface
{
    public function execute(\William\Base\Api\RequestInterface $request);
    public function setContext(\William\Base\Api\Framework\ListenerInterface $context): MiddlewareInterface;
}