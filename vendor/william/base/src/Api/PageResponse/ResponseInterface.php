<?php
declare(strict_types=1);

namespace William\Base\Api\PageResponse;

use William\Base\Api\AbstractResponse;

/**
 * Interface ResponseInterface
 *
 * @api
 * @package William\Base\Api\PageResponse
 */
interface ResponseInterface
{
    public function setVars(array $vars = []);
    public function setTemplate($template);
    public function getVars();
    public function getTemplate();
    public function toHtml();
    /**
     * @param string $blockName
     * @param string $block
     * @return $this
     */
    public function addChildBlock(string $blockName, string $block);
    /**
     * @param string $blockName
     * @return array
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function getChildBlocksHtml(string $blockName = '');
}