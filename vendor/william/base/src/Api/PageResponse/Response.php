<?php
declare(strict_types=1);

namespace William\Base\Api\PageResponse;

use William\Base\Api\AbstractResponse;

/**
 * Interface ResponseInterface
 *
 * @api
 * @package William\Base\Api\PageResponse
 */
class Response extends AbstractResponse implements ResponseInterface
{

}