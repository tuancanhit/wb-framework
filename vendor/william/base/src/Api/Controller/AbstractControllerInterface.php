<?php

namespace William\Base\Api\Controller;

/**
 * Class AbstractController
 *
 * @package William\Base\Controller
 */
interface AbstractControllerInterface
{
    public function execute();
    public function launch();
    public function setRedirect(string $handler);
    public function getRedirect();
    public function getRequest();
}