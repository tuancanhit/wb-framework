<?php

namespace William\Base\Api;

/**
 * Interface RequestInterface
 *
 * @api
 * @package William\Base\Controller
 */
interface RequestInterface
{
    public function getParam(string $key, $default = null);
    public function getParams();
    public function getMethod();
    public function getRequestPath();
    public function getFullPath();
    public function getScope();
    public function getUserIpAddress();
    public function isAdmin();
}