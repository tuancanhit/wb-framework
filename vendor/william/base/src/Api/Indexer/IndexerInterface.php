<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Api\Indexer;


/**
 * Interface IndexerInterface
 *
 * @api
 */
interface IndexerInterface
{
    public function setIndexer(string $indexer);

    public function getIndexer();

    public function reindexById($id);

    public function reindexAll();
}