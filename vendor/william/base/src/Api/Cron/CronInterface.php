<?php
/**
 * Copyright © William, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Api\Cron;

/**
 * Interface CronInterface
 *
 * @api
 */
interface CronInterface
{
    /**
     * @return mixed
     */
    public function execute();

    /**
     * @return mixed
     */
    public function isCompleted();

    /**
     * @return mixed
     */
    public function isFailure(?\Throwable $e = null);

    /**
     * @return string
     */
    public static function getSchedule(): string;

    /**
     * @return string
     */
    public function getCronIdentifier(): string;
}