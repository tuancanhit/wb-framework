<?php

declare(strict_types=1);

namespace William\Base\Framework\Cron;

use ReflectionException;
use William\Base\Api\Cron\CronInterface;
use William\Base\Helper\DependencyResolver;
use William\Base\Helper\ScopeResolver;
use William\Base\Logger\BaseLogger;
use William\Base\Model\AbstractInstance;
use William\Base\Observer\Event;

/**
 * Class CronHandler
 */
class CronHandler extends AbstractInstance
{
    const SCOPE_CODE = 'crontab';

    /** @var DependencyResolver */
    protected DependencyResolver $dependencyResolver;

    /** @var BaseLogger */
    protected BaseLogger $logger;

    /** @var Event  */
    protected Event $event;

    /**
     * @param array $data
     * @throws ReflectionException
     */
    public function __construct(array $data = [])
    {
        $this->dependencyResolver = DependencyResolver::getInstance();
        $this->logger = $this->dependencyResolver->resolve(BaseLogger::class);
        $this->event  = $this->dependencyResolver->resolve(Event::class);
        parent::__construct($data);
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    public function run()
    {
        (new ScopeResolver(self::SCOPE_CODE));
        $this->event->dispatch('core_before_cron_run', []);
        foreach ($this->getCronList() as $class) {
            if (!(new \ReflectionClass($class))->implementsInterface(CronInterface::class)) {
               continue;
            }
            try {
                if (!$this->isValidToRun($class)) {
                    continue;
                }
                /** @var CronInterface $cron */
                $cron = $this->dependencyResolver->resolve($class);
                $this->event->dispatch(
                    sprintf('%s_cron_start', $cron->getCronIdentifier()), ['current' => microtime(true)]
                );
                try {
                    $cron->execute();
                    $cron->isCompleted();
                } catch (\Exception | \Throwable | \TypeError $e) {
                    $cron->isFailure($e);
                    $this->event->dispatch(
                        sprintf('%s_cron_failure', $cron->getCronIdentifier()), [
                            'current'   => microtime(true),
                            'exception' => $e
                        ]
                    );
                    throw $e;
                }
                $this->event->dispatch(
                    sprintf('%s_cron_completed', $cron->getCronIdentifier()), ['current' => microtime(true)]
                );
            } catch (\Exception | \Throwable | \TypeError $e) {
                $this->logger->error($e->getMessage(), $e->getTrace());
            }
        }
        $this->event->dispatch('core_after_cron_run', []);
    }

    /**
     * @param \William\Base\Api\Cron\CronInterface $cronHandler
     * @return bool
     */
    public function isValidToRun($cronHandler)
    {
        $schedule = $cronHandler::getSchedule();
        $schedule = trim(str_replace('  ', ' ', $schedule));
        if ($schedule == '* * * * *') {
            return true;
        }
        // Support config cron schedule time later
        return false;
    }

    /**
     * @return array
     */
    public function getCronList()
    {
        $cronList = get_cron_list();
        asort($cronList);
        return $cronList;
    }
}