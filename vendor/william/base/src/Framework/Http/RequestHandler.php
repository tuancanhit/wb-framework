<?php

declare(strict_types=1);

namespace William\Base\Framework\Http;

use Exception;
use ReflectionException;
use Throwable;
use TypeError;
use William\Base\Api\Controller\AbstractControllerInterface;
use William\Base\Api\RequestInterface;
use William\Base\Controller\Ui\ErrorController;
use William\Base\Exception\AdminAuthenticateFailureException;
use William\Base\Exception\RouteNotFoundException;
use William\Base\Helper\DependencyResolver;
use William\Base\Helper\GlobalRequireResolver;
use William\Base\Logger\BaseLogger;
use William\Base\Model\AbstractInstance;
use William\Base\Helper\ScopeResolver;
use William\Base\Observer\Event;

/**
 * Class RequestHandler
 */
class RequestHandler extends AbstractInstance
{
    /** @var DependencyResolver */
    protected DependencyResolver $dependencyResolver;

    /** @var BaseLogger */
    protected BaseLogger $logger;

    /** @var Event  */
    protected Event $event;

    /** @var array  */
    protected array $routes = [];

    /**
     * @param array $data
     * @throws ReflectionException
     */
    public function __construct(array $data = [])
    {
        $this->dependencyResolver = DependencyResolver::getInstance();
        $this->logger = $this->dependencyResolver->resolve(BaseLogger::class);
        $this->event  = $this->dependencyResolver->resolve(Event::class);
        parent::__construct($data);
    }

    /**
     * @param RequestInterface $request
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function run(RequestInterface $request)
    {
        (ScopeResolver::newInstance($request->getScope()));
        try {
            $resolver = $this->resolveRequest($request);
            $this->event->dispatch('core_before_render', ['request' => $request, 'resolver' => &$resolver]);
            /** @var AbstractControllerInterface $controller */
            $controller = $this->dependencyResolver->resolve($resolver['handler']);
            echo $controller->launch();
        } catch (\Exception|TypeError|Throwable $e) {
            $this->logger->error($e->getMessage(), [
                'request' => $request->toJson(),
                'trace'   => $e->getTraceAsString()
            ]);
            if ($e instanceof AdminAuthenticateFailureException) {
                $this->logger->error('AdminAuthenticateFailure', [
                    'ip' => $request->getUserIpAddress()
                ]);
            }
            if (config('debug')) {
                throw $e;
            }
            echo $this->returnError($e->getMessage());
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function returnError(string $message)
    {
        $errorPage = config('site.error', ErrorController::class);
        $errorPage = $this->dependencyResolver->resolve($errorPage);
        if (method_exists($errorPage, 'setMessage')) {
            $errorPage->setMessage($message);
        }
        return $errorPage->launch();
    }

    /**
     * @param RequestInterface $request
     * @return array
     * @throws RouteNotFoundException
     */
    protected function resolveRequest(RequestInterface $request)
    {
        $routes = $this->getRoutes();
        $path   = $request->getFullPath();
        $method = strtolower($request->getMethod());
        if (!$path || !isset($routes[$path][$method])) {
            throw new RouteNotFoundException('Route not found.');
        }
        return $routes[$path][$method];
    }

    /**
     * @return array
     */
    protected function getRoutes()
    {
        if (empty($this->routes)) {
            $this->routes = GlobalRequireResolver::getInstance(WB_ROOT)->execute(
                '/src/route/web.php',
                '/package/*/route/web.php',
                '/vendor/william/*/src/route/web.php'
            );
        }
        return $this->routes;
    }
}