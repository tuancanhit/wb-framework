<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Framework;

use William\Base\Api\Framework\BootInterface;
use William\Base\Framework\Cron\CronHandler;
use William\Base\Framework\Http\RequestHandler;

/**
 * Class CronApplication
 *
 * @package William\Base\Framework
 */
class CronApplication extends Application
{
    /**
     * @param BootInterface $boot
     * @return Application
     */
    public static function create(\William\Base\Api\Framework\BootInterface $boot)
    {
        return new self(...func_get_args());
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function run()
    {
        (new CronHandler(['app' => $this]))->run();
    }
}