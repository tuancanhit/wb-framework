<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Framework;

use William\Base\Api\Framework\ConfigInterface;
use William\Base\Helper\GlobalRequireResolver;

/**
 * Class Config
 *
 * @package William\Base\Framework
 */
class Config implements ConfigInterface
{
    /** @var ConfigInterface|null  */
    protected static ?ConfigInterface $_instance = null;

    /** @var array|int[]|string[]  */
    protected array $configs = [];

    /**
     * @param string $root
     */
    public function __construct(protected string $root = WB_ROOT)
    {
        $this->configs = GlobalRequireResolver::getInstance($this->root)->execute(
            '/src/etc/config.php',
            '/package/*/etc/config.php',
            '/src/etc/tmpl.php',
            '/vendor/william/base/src/etc/config.php'
        );
        $this->configs['tmpl'] = GlobalRequireResolver::getInstance($this->root)
            ->execute('/package/*/etc/tmpl.php');
    }

    /**
     * @return ConfigInterface|Config
     */
    public static function getInstance(string $root = WB_ROOT)
    {
        if (null === self::$_instance) {
            self::$_instance = new self($root);
        }
        return self::$_instance;
    }

    /**
     * @param string $path
     * @param mixed $default
     * @return array|mixed|null
     */
    public function config(string $path = '', $default = null)
    {
        $config = $this->configs;
        if (!$path) {
            return $config;
        }
        foreach (explode('.', $path) as $node) {
            if (!isset($config[$node])) {
                return $default;
            } else {
                $config = $config[$node];
            }
        }
        return $config ?: $default;
    }

    /**
     * @param string $route
     * @param array  $params
     * @return string
     */
    function urlBuild(string $route, array $params = [])
    {
        if ($route[0] == '/') {
            $route = ltrim($route, $route[0]);
        }
        $baseUrl = $this->configs['site']['base_url'] ?? '/';
        if ($baseUrl[strlen($baseUrl) - 1] != '/') {
            $baseUrl = sprintf('%s/', $baseUrl);
        }
        if (empty($params)) {
            return sprintf('%s%s', $baseUrl, $route);
        }
        return sprintf('%s%s?%s', $baseUrl, $route, http_build_query($params));
    }
}