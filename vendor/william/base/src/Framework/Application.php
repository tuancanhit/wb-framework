<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Framework;

use William\Base\Api\Framework\AppInterface;
use William\Base\Api\Framework\BootInterface;
use William\Base\Controller\Request;
use William\Base\Exception\SystemInitFailureException;
use William\Base\Framework\Http\RequestHandler;

/**
 * Class Application
 *
 * @package William\Base\Framework
 */
class Application implements AppInterface
{
    /** @var Application|null  */
    public static ?Application $_instance = null;

    /** @var BootInterface  */
    protected BootInterface $boot;
    
    /**
     * @param BootInterface $boot
     */
    public function __construct(BootInterface $boot)
    {
        $this->boot = $boot;
        self::$_instance = $this;
    }

    /**
     * @return BootInterface|BootInterface|\William\Base\Framework\Boot
     */
    public function getBoot()
    {
        return $this->boot;
    }

    /**
     * @return $this
     * @throws SystemInitFailureException
     */
    public static function getInstance()
    {
        if (null == self::$_instance) {
            throw new SystemInitFailureException('System Init Failure');
        }
        return self::$_instance;
    }


    /**
     * @param BootInterface $boot
     * @return Application
     */
    public static function create(BootInterface $boot)
    {
        return new self(...func_get_args());
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function run()
    {
        $request = new Request();
        if (!$request->isCli()) {
            (new RequestHandler(['app' => $this]))->run($request);
        }
    }
}