<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Cache\File;

use William\Base\Model\DataObject;

/**
 * Class Json
 */
class Json
{
    /**
     * @param string $cacheId
     * @return string
     */
    protected function getRootCachePath(string $cacheId)
    {
        return sprintf('%s/var/cache/%s.json', WB_ROOT, $cacheId);
    }

    /**
     * @param string $cacheId
     * @return string
     */
    public function get(string $cacheId)
    {
        $path = $this->getRootCachePath($cacheId);
        if (!file_exists($path)) {
            return '';
        }
        return file_get_contents($path);
    }

    /**
     * @param string $cacheId
     * @param        $data
     * @return bool
     */
    public function set(string $cacheId, $data)
    {
        $path = $this->getRootCachePath($cacheId);
        if (is_array($data)) {
            $data = json_encode($data, 128);
        }
        if ($data instanceof DataObject) {
            $data = $data->toJson();
        }
        return (bool)file_put_contents($path, $data);
    }
}