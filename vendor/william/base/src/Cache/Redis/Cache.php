<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Cache\Redis;

use Predis\Cluster\Distributor\EmptyRingException;
use William\Base\Model\AbstractInstance;
use William\Base\Model\DataObject;

/**
 * Class AbstractData
 */
class Cache extends \Predis\Client
{
    protected \Predis\Client $redis;

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->redis = new \Predis\Client([
            'scheme' => 'tcp',
            'host' => config('redis.host'),
            'port' => config('redis.port'),
        ]);

        parent::__construct($data);
    }

    /**
     * @param string $identifier
     * @param mixed  $data
     * @return \Predis\Response\Status|null
     */
    public function set(string $identifier, $data)
    {
        $dataType = $this->getDataType($data);
        if ($data instanceof DataObject) {
            $data = $data->getData();
        }
        if ($data instanceof \stdClass) {
            $data = (array)$data;
        }
        return $this->redis->set($identifier, json_encode(
            [
                'type' => $dataType,
                'raw_data' => $data
            ]
        ));
    }

    /**
     * @param $data
     * @return string
     */
    protected function getDataType($data)
    {
        if (is_string($data)) {
            return 'string';
        }
        if (is_numeric($data)) {
            return 'number';
        }
        if (is_object($data)) {
            return get_class($data);
        }
        if (is_array($data)) {
            return 'array';
        }
        return '';
    }

    /**
     * @param string $identifier
     * @return mixed
     * @throws EmptyRingException
     */
    public function get(string $identifier)
    {
        $res = $this->redis->get($identifier);
        if (empty($data)) {
            throw new EmptyRingException('Empty');
        }
        return $res['raw_data'];
    }
}