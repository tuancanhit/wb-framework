<?php
declare(strict_types=1);

namespace William\Base\Indexer;

use William\Base\Api\Indexer\IndexerInterface;
use William\Base\Helper\DependencyResolver;
use William\Base\Model\AbstractInstance;
use William\Base\Model\DataObject;

/**
 * Class Indexer
 *
 * @package William\Base\Indexer
 */
class Indexer implements IndexerInterface
{
    protected string $indexer;
    protected array $indexers;
    protected DependencyResolver $dependencyResolver;

    public function __construct(DependencyResolver $dependencyResolver)
    {
        $this->dependencyResolver = $dependencyResolver;
    }

    public function setIndexer(string $indexer)
    {
        $this->indexer = $indexer;
        return $this;
    }

    /**
     * @throws \ReflectionException
     */
    public function getIndexer()
    {
        if (empty($this->indexers[$this->indexer])) {
            $this->indexers[$this->indexer] = $this->dependencyResolver->resolve($this->indexer);
        }
        return $this->indexers[$this->indexer];
    }

    public function reindexById($id)
    {
        return $this->getIndexer()->exectute($id);
    }

    public function reindexAll()
    {
        return $this->getIndexer()->exectute();
    }
}