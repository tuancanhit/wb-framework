<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Block\Element;

use William\Base\Block\AbstractBlock;

/**
 * Class Header
 */
class Header extends AbstractBlock
{
    protected string $identifier = 'core_header';
    protected string $template = '@core::element/header.php';
}