<?php

namespace William\Base\Block;

use William\Base\Api\AbstractResponse;
use William\Base\Helper\DependencyResolver;
use William\Base\Helper\TemplateResolver;
use William\Base\Model\AbstractInstance;

/**
 * Class AbstractBlock
 *
 * @package William\Base\Block
 */
class AbstractBlock extends AbstractResponse implements BlockInterface
{
    /** @var string  */
    protected string $template = 'general';
    protected string $identifier = '';

    /**
     * @return void
     */
    protected function afterConstruct(array $args = [])
    {
        if ($this->template) {
            $this->setTemplate($this->template);
        }
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getVars()
    {
        $this->event->dispatch(sprintf('block_%s_get_vars_before', $this->identifier), ['object' => $this]);
        $vars = parent::getVars();
        $this->event->dispatch(sprintf('block_%s_get_vars_after', $this->identifier), ['object' => $this, 'vars' => &$vars]);
        return $vars;
    }
}