<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Block;

use William\Base\Helper\DependencyResolver;

/**
 * Class Renderer
 *
 * @package William\Base\Block
 */
class Renderer
{
    /**
     * @param DependencyResolver $dependencyResolver
     */
    public function __construct(protected DependencyResolver $dependencyResolver){}

    /**
     * @param string $block
     * @return AbstractBlock
     * @throws \ReflectionException
     */
    public function createBlock(string $block)
    {
        return $this->dependencyResolver->resolve($block);
    }
}