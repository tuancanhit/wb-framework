<?php
use William\Base\Controller\Request;
$url = urlBuild((new Request())->getFullPath(), ['is_ajax_request_render' => 1]);
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<div id="content"></div>
<script>
    $(document).ready(function () {
        $.ajax({
            url: "<?= $url ?>",
            success: function (result) {
                $("#content").html(result);
            }
        });
    });
</script>
