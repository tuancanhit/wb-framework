<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Error Page</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .error-container {
            text-align: center;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
        }
        h1 {
            color: #ff0000;
        }
    </style>
</head>
<body>
<?php
$error = 'We\'re sorry, but it seems there was an error.';
if (isset($message)) {
    $error = $message;
} ?>
<div class="error-container">
    <h1>Oops! Something went wrong.</h1>
    <p><?= $error ?></p>
    <p>Please try again later.</p>
</div>
</body>
</html>
