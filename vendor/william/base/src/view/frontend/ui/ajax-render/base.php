<?php ?>
<content id="content"></content>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            $.ajax({
                url: "<?= $ui_render_url ?>",
                success: function (content) {
                    $('#content').html(content);
                }
            });
        }, 0);
    });
</script>