<div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
        <?php if (!empty($navs)): ?>
            <?php foreach ($navs as $nav) :?>
            <a class="p-2 text-muted" href="<?= $nav->getUrl() ?>"><?= $nav->getTitle() ?></a>
            <?php endforeach; ?>
        <?php endif; ?>
    </nav>
</div>