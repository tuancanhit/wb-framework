<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Helper;

/**
 * Class GlobalRequireResolver
 *
 * @package William\Base\Helper
 */
class GlobalRequireResolver
{
    protected static ?GlobalRequireResolver $instance = null;

    /**
     * @param string $root
     */
    public function __construct(protected string $root = WB_ROOT)
    {}

    /**
     * @param string $root
     * @return GlobalRequireResolver|null
     */
    public static function getInstance(string $root = WB_ROOT)
    {
        if (null == self::$instance) {
            self::$instance = new self($root);
        }
        return self::$instance;
    }

    /**
     * @param ...$paths
     * @return array
     */
    public function execute(...$paths)
    {
        $result = [];
        foreach ($paths as $path) {
            foreach (glob($this->root . $path) as $filename) {
                $tmp = require $filename;
                $result = array_merge_recursive($result, $tmp);
            }
        }
        return $result;
    }
}