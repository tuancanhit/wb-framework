<?php

namespace William\Base\Helper;

use William\Base\Exception\SystemInitFailureException;

/**
 * Class ScopeResolver
 *
 * @package William\Base\Helper
 */
class ScopeResolver
{
    const IS_ADMIN = 'is_admin';
    const IS_FRONT = 'is_front';
    const IS_CLI   = 'is_cli';

    const TEMPLATE_DIR = [
        self::IS_ADMIN => 'admin',
        self::IS_FRONT => 'frontend',
        self::IS_CLI   => 'cli'
    ];

    /** @var string  */
    protected string $scope = '';

    /** @var ScopeResolver|null  */
    protected static ?ScopeResolver $_instance = null;

    /**
     * @param string $scope
     */
    public function __construct(string $scope = '')
    {
        $this->scope = $scope;
        self::$_instance = $this;
    }

    /**
     * @param string $scope
     * @return ScopeResolver
     * @throws SystemInitFailureException
     */
    public static function newInstance(string $scope = '')
    {
        if (null !== self::$_instance) {
            throw new SystemInitFailureException('Just init scope once time.');
        }
        return self::$_instance = new self(...func_get_args());
    }

    /**
     * @return ScopeResolver
     * @throws SystemInitFailureException
     */
    public static function getInstance(): ScopeResolver
    {
        if (null == self::$_instance) {
            throw new SystemInitFailureException('Scope does not init');
        }
        return self::$_instance;
    }

    /**
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     * @return string
     * @throws \Exception
     */
    public function getScopeTemplateDir(string $scope = '')
    {
        $scope = $scope ?: $this->getScope();
        if (empty(self::TEMPLATE_DIR[$scope])) {
            throw new \Exception(sprintf('Template scope %s is not defined.', $scope));
        }
        return self::TEMPLATE_DIR[$scope];
    }
}