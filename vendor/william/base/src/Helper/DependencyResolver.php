<?php

namespace William\Base\Helper;

use Exception;
use ReflectionClass;
use William\Base\Exception\InstanceNotFoundException;
use William\Base\Pool\AbstractPool;

/**
 * Class DependencyResolver
 *
 * @package William\Base
 */
class DependencyResolver
{
    /** @var null | $this */
    public static $_instance = null;

    /** @var array */
    private $dependencies = [];

    /** @var \William\Base\Pool\DependencyInjectionPool */
    private $dependencyPool;

    /** @var array  */
    private array $references = [];

    /** DependencyResolver */
    public function __construct()
    {
        $this->dependencyPool = AbstractPool::getInstance();
    }

    /**
     * @return DependencyResolver|null
     */
    public static function getInstance()
    {
        if (null == self::$_instance) {
            self::$_instance = new self();
            self::$_instance->setDependencyArgs([
                self::class => self::$_instance
            ]);
        }
        return self::$_instance;
    }

    /**
     * @param array $dependencies
     * @return $this
     */
    public function setDependencyArgs(array $dependencies = [])
    {
        $this->dependencies = array_merge($this->dependencies, $dependencies);
        return $this;
    }

    /**
     * @return $this
     */
    public function clearDependencyArgs()
    {
        $self = $this->dependencies[self::class] ?: null;
        $this->dependencies = [];
        if ($self) {
            $this->dependencies[self::class] = $self;
        }
        return $this;
    }

    /**
     * @param string $name
     * @param mixed  $value
     * @return $this
     */
    public function replaceDependencyArgs(string $name, $value)
    {
        $this->dependencies[$name] = $value;
        return $this;
    }

    /**
     * @param string $className
     * @return mixed|object|null
     * @throws \ReflectionException
     * @throws Exception
     */
    public function resolve(string $className)
    {
        $className = $this->getPreference($className);
        $cachedInstance = $this->getFromPool($className);
        if (null !== $cachedInstance) {
            return $cachedInstance;
        }
        $instance = $this->_resolve(...func_get_args());
        $this->dependencyPool->addObject($className, $instance);
        return $instance;
    }

    /**
     * @param string $className
     * @return mixed|object|null
     * @throws \ReflectionException
     * @throws Exception
     */
    protected function _resolve(string $className)
    {
        $reflection = new ReflectionClass($className);
        $constructor = $reflection->getConstructor();
        if (!$constructor) {
            return new $className();
        }
        $dependencies = [];
        foreach ($constructor->getParameters() as $parameter) {
            $name = $parameter->getName();
            $className = $parameter->getType() ? $parameter->getType()->getName() : '';
            $isDefaultValueAvailable = $parameter->isDefaultValueAvailable();
            $className = $this->getPreference($className);
            if (!$isDefaultValueAvailable && isset($this->dependencies[$className])) {
                $dependencies[$name] = $this->dependencies[$className];
            } elseif ($this->isSelf($parameter)) {
                $self = $this->dependencies[$className] ?: null;
                if (null == $self) {
                    $self = self::getInstance();
                    $this->dependencies[$className] = $self;
                }
                $dependencies[$name] = $self;
            } else {
                $dependencyClass = $parameter->getName();
                if (!$dependencyClass) {
                    $dependencyValue = $parameter->getDefaultValue();
                } else {
                    if ($isDefaultValueAvailable) {
                        $dependencyValue = $parameter->getDefaultValue();
                    } else {
                        $refClass = $parameter->getType() && !$parameter->getType()->isBuiltin()
                            ? new ReflectionClass($parameter->getType()->getName())
                            : null;
                        $dependencyValue = $refClass ? $this->_resolve($refClass->getName()) : null;
                    }
                }
                $dependencies[$name] = $dependencyValue;
                $this->dependencies[$className] = $dependencyValue;
            }
        }

        return $reflection->newInstanceArgs($dependencies);
    }

    /**
     * @param \ReflectionParameter $parameter
     * @return bool
     */
    protected function isSelf(\ReflectionParameter $parameter)
    {
        return $parameter->getName() === self::class;
    }

    /**
     * @param string $class
     * @return object|null
     */
    protected function getFromPool(string $class)
    {
        try {
            return $this->getPool()->getObject($class, false);
        } catch (InstanceNotFoundException $e) {
            return null;
        }
    }

    /**
     * @return \William\Base\Pool\DependencyInjectionPool
     */
    public function getPool()
    {
        return $this->dependencyPool;
    }

    /**
     * @param string $ref
     * @return string
     */
    protected function getPreference(string $ref)
    {
        $references = $this->getPreferences();
        if (!empty($references[$ref])) {
            return $references[$ref];
        }
        return $ref;
    }

    /**
     * @return array
     */
    protected function getPreferences()
    {
        if (empty($this->references)) {
            $this->references = GlobalRequireResolver::getInstance(WB_ROOT)->execute(
                '/vendor/william/*/src/etc/di.php',
                '/package/*/etc/di.php',
                '/src/etc/di.php',
            );
        }
        return $this->references;
    }
}
