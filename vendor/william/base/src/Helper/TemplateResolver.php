<?php
declare(strict_types=1);

namespace William\Base\Helper;

use William\Base\Cache\File\Json;
use William\Base\Exception\SystemInitFailureException;
use William\Base\Framework\Config;
use William\Base\Framework\HttpApplication;
use William\Base\Model\AbstractInstance;

/**
 * Class ConfigResolver
 */
class TemplateResolver extends AbstractInstance
{
    private string $cacheId = 'json_tmpl_mapping';

    /** @var TemplateResolver|null */
    private static ?TemplateResolver $_instance = null;

    /** @var array */
    private array $mappings = [];

    /** @var string */
    public static string $_appTplPrefix = '@app';

    /** @var string */
    public static string $_coreTplPrefix = '@core';

    /**
     * @param Config $configResolver
     * @param Json   $jsonCache
     * @param array  $data
     * @param string $scope
     * @throws SystemInitFailureException
     */
    public function __construct(
        Config         $configResolver,
        protected Json $jsonCache,
        array          $data = [],
        string         $scope = ''
    )
    {
        $rootDir = HttpApplication::getInstance()->getBoot()->getRootDir();
        $tmplDir = ScopeResolver::getInstance()->getScopeTemplateDir($scope);
        if (!$tmplDir || !$rootDir) {
            throw new SystemInitFailureException('Scope or root dir not found');
        }
        $this->mappings = $this->getFromJsonCache();
        if (empty($this->mappings)) {
            $this->mappings = [
                self::$_appTplPrefix => '%s/src/view/%s',
                self::$_coreTplPrefix => '%s/vendor/william/base/src/view/%s'
            ];

            $tmpl = $configResolver->config('tmpl');
            if (is_array($tmpl)) {
                $this->mappings = array_merge($this->mappings, $tmpl);
            }
            $this->mappings = $this->resolveTemplates($this->mappings, $rootDir, $tmplDir);
            uksort($this->mappings, [$this, 'strLengthCompare']);

            $this->jsonCache->set($this->cacheId, $this->mappings);
        }
        parent::__construct(array_merge($data, ['scope' => $scope]));
        self::$_instance = $this;
    }

    /**
     * @return array
     */
    protected function getFromJsonCache()
    {
        $content = $this->jsonCache->get($this->cacheId);
        if (!$content) {
            return [];
        }
        return json_decode($content, true);
    }

    /**
     * @param array  $templates
     * @param string $rootDir
     * @param string $tmplDir
     * @return array
     */
    protected function resolveTemplates(array $templates, string $rootDir, string $tmplDir)
    {
        foreach ($templates as $index => $tmpl) {
            if (str_starts_with($tmpl, '/')) {
                $tmpl = substr($tmpl, 1);
            }
            if (!str_starts_with($tmpl, '%s')) {
                $tmpl = sprintf('%s/%s', '%s', $tmpl);
            }
            if (str_ends_with($tmpl, '/')) {
                $tmpl = substr_replace($tmpl, '', -1);
            }
            if (!str_ends_with($tmpl, '%s')) {
                $tmpl = sprintf('%s/%s', $tmpl, '%s');
            }
            if (!str_ends_with($tmpl, '/')) {
                $tmpl = sprintf('%s/', $tmpl);
            }
            $templates[$this->mask($index)] = sprintf($tmpl, $rootDir, $tmplDir);
            unset($templates[$index]);
        }

        return $templates;
    }

    /**
     * @param string $area
     * @return string
     */
    protected function mask(string $area)
    {
        return sprintf('%s::', $area);
    }

    /**
     * @throws SystemInitFailureException
     */
    public static function getInstance()
    {
        if (null == self::$_instance) {
            throw new SystemInitFailureException('System have not init already');
        }
        return self::$_instance;
    }

    /**
     * @param string $template
     * @return string
     * @throws \Exception
     */
    public function resolve(string $template)
    {
        $area = $this->getArea($template);
        return str_replace(
            $area, $this->mappings[$area], $template
        );
    }

    /**
     * @param string $template
     * @return string
     * @throws \Exception
     */
    protected function getArea(string $template)
    {
        $template = strtolower($template);
        foreach (array_keys($this->mappings) as $scope) {
            $length = strlen($scope);
            if (substr($template, 0, $length) == $scope) {
                return $scope;
            }
        }
        throw new \Exception('Template area not found');
    }

    /**
     * @param string $a
     * @param string $b
     * @return int
     */
    public function strLengthCompare($a, $b)
    {
        return strlen($a) < strlen($b) ? 1 : 0;
    }
}