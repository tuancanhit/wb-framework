<?php

declare(strict_types=1);

namespace William\Base\Middleware;

use William\Base\Controller\Ui\AjaxRenderController;
use William\Base\Helper\DependencyResolver;
use William\Base\Logger\BaseLogger;
use William\Base\Observer\Listener;

/**
 * Class AjaxRender
 *
 * @package William\Base\Middleware
 */
class AjaxRender extends Listener
{
    /**
     * @var BaseLogger
     */
    protected BaseLogger $logger;

    /**
     * @param DependencyResolver $dependencyResolver
     * @param BaseLogger         $logger
     */
    public function __construct(DependencyResolver $dependencyResolver, BaseLogger $logger)
    {
        $this->logger = $logger;
        parent::__construct($dependencyResolver);
    }

    /**
     * @param array|null $args
     * @return void
     */
    public function execute(array &$args = null)
    {
        $resolver = empty($args['resolver']['handler']) ? null : $args['resolver']['handler'];
        if ($args['request']->getIsAjaxRequestRender() || empty($resolver) || !class_exists($resolver) || !$resolver::IS_AJAX_RENDER) {
            return;
        }
        $args['resolver']['handler'] = AjaxRenderController::class;
    }
}