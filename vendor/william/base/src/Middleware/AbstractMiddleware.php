<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Middleware;

use William\Base\Api\Framework\ListenerInterface;
use William\Base\Api\Framework\MiddlewareInterface;
use William\Base\Api\RequestInterface;

/**
 * Class AbstractMiddleware
 *
 * @package William\Base\Middleware
 */
abstract class AbstractMiddleware implements MiddlewareInterface
{
    protected ?ListenerInterface $context = null;

    abstract public function execute(RequestInterface $request);

    public function setContext(ListenerInterface $context): MiddlewareInterface
    {
        $this->context = $context;
        return $this;
    }
}