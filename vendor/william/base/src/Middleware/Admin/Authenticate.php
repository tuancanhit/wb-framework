<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Middleware\Admin;

use William\Base\Api\RequestInterface;
use William\Base\Middleware\AbstractMiddleware;

/**
 * Class Authenticate
 *
 * @package William\Base\Middleware\Admin
 */
class Authenticate extends AbstractMiddleware
{
    public function execute(RequestInterface $request)
    {
        if (!$request->isAdmin()) {
            return;
        }
    }
}