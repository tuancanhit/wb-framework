<?php

declare(strict_types=1);

namespace William\Base\Middleware;

use William\Base\Api\Framework\MiddlewareInterface;
use William\Base\Exception\MiddlewareException;
use William\Base\Helper\DependencyResolver;
use William\Base\Logger\BaseLogger;
use William\Base\Observer\Listener;

/**
 * Class MiddlewareResolver
 *
 * @package William\Base\Middleware
 */
class MiddlewareResolver extends Listener
{
    /**
     * @var BaseLogger
     */
    protected BaseLogger $logger;

    /**
     * @param DependencyResolver $dependencyResolver
     * @param BaseLogger         $logger
     */
    public function __construct(DependencyResolver $dependencyResolver, BaseLogger $logger)
    {
        $this->logger = $logger;
        parent::__construct($dependencyResolver);
    }

    /**
     * @param array|null $args
     * @return void
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function execute(array &$args = null)
    {
        $request  = $args['request'];
        $resolver = $args['resolver'];

        if (empty($resolver['middleware'])) {
            return;
        }
        $middleware = $resolver['middleware'];
        $middleware = is_array($middleware) ? $middleware : [$middleware];
        foreach ($middleware as $service) {
            if (is_callable($service)) {
                $service($request);
                return;
            }
            if (is_string($service) && class_exists($service)) {
                if (!is_subclass_of($service, MiddlewareInterface::class)) {
                    throw new MiddlewareException(sprintf('Middleware must be implement %s', '\William\Base\Api\Framework\MiddlewareInterface'));
                }
                /** @var MiddlewareInterface $ins */
                $ins = $this->dependencyResolver->resolve($service);
                $ins->setContext($this)->execute($request);
            } else {
                throw new MiddlewareException(sprintf('Middleware %s is not defined', $service));
            }
        }
    }
}