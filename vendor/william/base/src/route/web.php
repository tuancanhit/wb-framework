<?php

use William\Base\Route\Router;

$router = new Router();

return $router->getRoutes();