<?php
declare(strict_types=1);

namespace William\Base\Model;

use William\Base\Helper\DependencyResolver;

/**
 * Class AbstractInstance
 *
 * @package William\Base\Model
 */
class AbstractInstance extends DataObject
{
    /**
     * @return AbstractModel
     * @throws \ReflectionException
     */
    public function create()
    {
        return DependencyResolver::getInstance()->resolve(get_class($this));
    }

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->beforeConstruct(func_get_args());
        parent::__construct($data);
        $this->afterConstruct(func_get_args());
    }

    /**
     * @return void
     */
    protected function afterConstruct(array $args = []){}

    /**
     * @return void
     */
    protected function beforeConstruct(array $args = []){}
}