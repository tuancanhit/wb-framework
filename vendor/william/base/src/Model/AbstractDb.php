<?php

namespace William\Base\Model;

use William\Base\Api\DbConnector;
use William\Base\Api\DbConnectorInterface;
use William\Base\Exception\SystemInitFailureException;

/**
 * Class AbstractDb
 *
 * @package William\Base\Model
 */
abstract class AbstractDb extends \William\Base\Model\AbstractInstance
{
    /**
     * @var DbConnector|DbConnectorInterface
     */
    private ?DbConnector $dbConnector = null;

    /**
     * @var string
     */
    protected string $class = '';

    /**
     * @throws SystemInitFailureException
     */
    public function __construct(array $data)
    {
        if (null == $this->dbConnector) {
            try {
                $this->dbConnector = DbConnector::getInstance();
            } catch (SystemInitFailureException $e) {
                $this->dbConnector = (new DbConnector(config('database')));
            }
        }
        $this->class = get_class($this);
        parent::__construct($data);
    }

    /**
     * @return DbConnector|DbConnectorInterface|null
     */
    protected function getDbConnector()
    {
        return $this->dbConnector;
    }

    /**
     * @return string
     */
    public function getMainTable()
    {
        return $this->getData('table');
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getData('primary');
    }
}