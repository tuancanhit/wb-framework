<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Observer;

use William\Base\Helper\DependencyResolver;
use William\Base\Model\AbstractInstance;
use William\Base\Model\DataObject;

/**
 * Class Listener
 *
 * @package William\Base\Observer
 */
abstract class Listener implements \William\Base\Api\Framework\ListenerInterface
{
    /** @var string  */
    protected string $event = '';

    /** @var DataObject  */
    protected DataObject $args;

    protected ?DependencyResolver $dependencyResolver = null;

    /**  */
    public function __construct(DependencyResolver $dependencyResolver)
    {
        $this->dependencyResolver = $dependencyResolver;
    }

    /**
     * @param array|null $args
     * @return mixed
     */
    abstract public function execute(array &$args = null);

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     * @return $this
     */
    public function setEvent(string $event)
    {
        $this->event = $event;
        return $this;
    }
}