<?php

declare(strict_types=1);

namespace William\Base\Observer\Security;

use William\Base\Controller\Request;
use William\Base\Helper\DependencyResolver;
use William\Base\Logger\BaseLogger;
use William\Base\Observer\Listener;

/**
 * Class AccessLogListener
 */
class FormKeyListener extends Listener
{
    /**
     * @var BaseLogger
     */
    protected BaseLogger $logger;

    /**
     * @param DependencyResolver $dependencyResolver
     * @param BaseLogger         $logger
     */
    public function __construct(DependencyResolver $dependencyResolver, BaseLogger $logger)
    {
        $this->logger = $logger;
        parent::__construct($dependencyResolver);
    }

    /**
     * @param array|null $args
     * @return void
     */
    public function execute(array &$args = null)
    {
        $args['vars']['form_key'] = rand(1000, 100000);
    }
}