<?php
/**
 * Copyright © Will, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace William\Base\Observer;

use William\Base\Api\Framework\ListenerInterface;
use William\Base\Helper\DependencyResolver;
use William\Base\Helper\GlobalRequireResolver;

/**
 * Class Event
 *
 * @package William\Base\Observer
 */
class Event
{
    /** @var DependencyResolver  */
    protected DependencyResolver $dependencyResolver;
    /** @var array  */
    protected array $events = [];

    public function __construct()
    {
        $this->dependencyResolver = DependencyResolver::getInstance();
    }

    /**
     * @param string     $event
     * @param array|null $args
     * @return void
     * @throws \ReflectionException
     */
    public function dispatch(string $event, array $args = null)
    {
        $this->_dispatch($event, $args);
    }

    /**
     * @param string $event
     * @param array|null $args
     * @return void
     * @throws \ReflectionException
     */
    protected function _dispatch(string $event, array &$args = null)
    {
        $listeners = $this->getEvent($event);
        foreach ($listeners as $listener) {
            if (!is_subclass_of($listener, ListenerInterface::class)) {
                continue;
            }
            $listener = $this->dependencyResolver->resolve($listener);
            $listener->setEvent($event)->execute($args);
        }
    }

    /**
     * @param string $event
     * @return array
     */
    protected function getEvent(string $event)
    {
        $events = $this->getEvents();
        if (isset($events[$event])) {
            return $events[$event];
        }
        return [];
    }

    /**
     * @return array
     */
    protected function getEvents()
    {
        if (empty($this->events)) {
            $this->events = GlobalRequireResolver::getInstance(WB_ROOT)->execute(
                '/src/etc/events.php',
                '/package/*/etc/events.php',
                '/vendor/william/*/src/etc/events.php'
            );
        }
        return $this->events;
    }
}