<?php

return [
    'core_after_render' => [
        \William\Base\Observer\Security\AccessLogListener::class
    ],
    'core_before_render' => [
        \William\Base\Middleware\MiddlewareResolver::class,
        \William\Base\Middleware\AjaxRender::class
    ],
    'block_login_form_get_vars_after' => [
        \William\Base\Observer\Security\FormKeyListener::class
    ]
];