<?php
declare(strict_types=1);

namespace William\Base\Exception;

/**
 * Class AdminAuthenticateFailureException
 *
 * @package William\Base\Exception
 */
class AdminAuthenticateFailureException extends \Exception
{

}