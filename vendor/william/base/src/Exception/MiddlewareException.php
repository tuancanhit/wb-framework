<?php
declare(strict_types=1);

namespace William\Base\Exception;

/**
 * Class MiddlewareException
 *
 * @package William\Base\Exception
 */
class MiddlewareException extends \Exception
{

}